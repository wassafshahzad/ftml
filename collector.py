import pandas as pd
from alpha_vantage.foreignexchange import ForeignExchange
import time

key = 'DHM2G6ISH1LCYV9C'
cc = ForeignExchange(key=key,output_format='pandas')

def get_data():
    dataframe,_ = cc.get_currency_exchange_intraday(from_symbol='EUR',to_symbol='USD')
    return dataframe

def save_data(dataframe):
    dataframe.to_csv("EURO_VS_USD")

def load_data():
    previous_dataframe = pd.read_csv("EURO_VS_USD")
    return previous_dataframe
   
def is_dataframe_empty(dataframe):
    return dataframe.shape[0]==0


def collect_daily_data():
    dataframe = pd.DataFrame()
    days = 0
    while days <= 1 :
        daily_dataframe  = get_data()
        if(is_dataframe_empty(dataframe)):
            dataframe  =  daily_dataframe.copy()
        else:
            dataframe.append(daily_dataframe)
        days = days + 1
        time.sleep(86400)
    save_data.(dataframe)   
collect_daily_data()